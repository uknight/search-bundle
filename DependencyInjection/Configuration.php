<?php

namespace Uknight\SearchBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('uknight_search');

        $treeBuilder->getRootNode()
            ->children()
                ->integerNode('limit')->defaultValue(10)->end()
                ->arrayNode('entities')
                    ->useAttributeAsKey('short_name')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('entity')->cannotBeEmpty()->end()
                            ->integerNode('limit')->defaultValue('%uknight_search.limit%')->end()
                            ->scalarNode('title')->cannotBeEmpty()->end()
                            ->scalarNode('item_template')->cannotBeEmpty()->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
