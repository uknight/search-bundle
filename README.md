# U-Knight Search Bundle

Simple configurable helper for site search

## Instruction

### Prerequisites

You need:

1. symfony
2. doctrine
3. twig
4. brain and straight arms
5. this installation instructions

### Installation

Add to your ```composer.json``` file following lines:

```json
    ...
    "repositories": [
        {
            "url": "git@bitbucket.org:uknight/search-bundle.git",
            "type": "git"
        }
    ]
    ...
```

```json
    "require": {
        ...
        "uknight/search-bundle": "@dev"
    },
```

Then run

```bash
$ composer install
```

### Configuration

Add following lines to your ```config/bundles.php``` file:

```php
return [
    ...
    Uknight\SearchBundle\UknightSearchBundle::class => ['all' => true],
];
```

Add ```config/bundles/uknight_search.yaml``` with such a content:

```yaml
uknight_search:
    limit:                10 # default limit for all entities
    entities:

        # Prototype
        short_name: # short name for an entity
            entity:               Acme\FooBundle\Entity\Product
            limit:                '%uknight_search.limit%'
            title:                'Acme foo'
            item_template:        'AcmeFooBundle:Item:item.html.twig' # template for a single item

```


## Credits

Big thanks to myself, Flash from U-Knight Web Studio LLC [u-knight.de](https://u-knight.de/en/) , for making this bundle ;)