

(function(){
    var loadMoreForms = [].slice.call(document.querySelectorAll('[id^=more-]'));
    loadMoreForms.forEach(function (form) {
        document.addEventListener('submit', function(e) {
          
          console.log(e.target.nodeName);
          console.log(e.target);
          console.log(form);
          
          if (e.target === form) {
            e.preventDefault();
            send(e);
          }
        });
    });

    /**
     * Merges two object's options and returns new object
     * Needed for ajax function
     *
     * @returns newObject a new object based on input objects
     */
    function merge_options(){
      if (arguments.length < 2) {
          throw new Error('Not enough arguments. Expected 2 or more');
      }
      var newObject = {};
      var args = Array.from(arguments);
      args.forEach(function (obj) {
          for (var attrName in obj) {
              newObject[attrName] = obj[attrName];
          }
      });
      return newObject;
    }
    /**
     * Serialize form to formData
     * 
     * @param {HTMLElement} form Form HTML Element
     */
    window.serialize = function(form) {
      var formData = new FormData(form);
      return formData;
    }

    function ajax (options) {
      var XHR = new XMLHttpRequest();
      var defaultOptions = {
          url: null,
          data: null,
          type: 'get',
          dataType: 'json',
          contentType: 'json',
          beforeSend: function () {},
          success: function () {},
          error: function () {},
          complete: function () {}
      };

      options = merge_options(defaultOptions, options);

      // Define what happens on successful data submission
      XHR.addEventListener("load", function(event) {
          if (this.status >= 200 && this.status < 400) {
              // Success!
              var resp = this.response;
              switch (options.dataType) {
                  case 'html':
                      break;
                  case 'json':
                  default:
                      resp = JSON.parse(resp);
                      break;
              }
              options.success(resp);
          } else {
            var resp = event.target.responseText;
            switch (options.dataType) {
              case 'html':
                  break;
              case 'json':
              default:
                  resp = JSON.parse(event.target.responseText);
                  break;
            }
            options.error(resp);
          }
      });

      XHR.addEventListener("loadend", options.complete);

      // Define what happens in case of error
      XHR.addEventListener("error", options.error);

      options.beforeSend();

      // Set up our request
      XHR.open(options.type.toUpperCase(), options.url);
      switch (options.contentType) {
        case 'form':
          XHR.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
          break;
        case 'multipart':
          // XHR.setRequestHeader('Content-Type', 'multipart/form-data');
          break;
        case 'json':
        default:
          XHR.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
          break;
      }

      // The data sent is what the user provided in the form
      XHR.send(options.data);
    }

    function send(e) {
        var form = e.target;
        ajax({
            url: form.action,
            data: serialize(form),
            dataType: 'html',
            contentType: 'form',
            type: 'post',
            success: function (res) {
                form.parentNode.parentNode.outerHTML = res;
            },
            error: function (err) {
                console.log(err);
            }
        });
    }

})();

