<?php

namespace Uknight\SearchBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/search", name="uknight_search")
     */
    #[Route("/search", name: "uknight_search")]
    public function searchAction(Request $request, EntityManagerInterface $em, TranslatorInterface $trans)
    {
        $string = $request->get('s');

        $entities = $this->getParameter('uknight_search.entities');

        foreach ($entities as $name => $params) {
            $result = $em
                ->getRepository($params['entity'])
                ->search($string, $params['limit'])
                ;

            list($entities[$name]['items'], $entities[$name]['count']) = $result;
        }

        return $this->render('@UknightSearch/Search/search.html.twig', array(
            'string'  => $string,
            'results' => $entities,
            'locale'  => $trans->getLocale(),
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/search/more", name="uknight_search_load_more")
     */
    #[Route("/search/more", name: "uknight_search_load_more")]
    public function loadMoreAction(Request $request, EntityManagerInterface $em, TranslatorInterface $trans)
    {
        $string = $request->get('s');
        $entity = $request->get('entity');
        $page   = $request->get('page');

        $entities = $this->getParameter('uknight_search.entities');

        foreach ($entities as $name => $params) {
            if ($entity === $name) {
                $entity = $params;
                $entityName = $name;

                $result = $em
                    ->getRepository($params['entity'])
                    ->search($string, $params['limit'], $page)
                    ;

                list($entity['items'], $entity['count']) = $result;
            }
        }
        
        return $this->render('@UknightSearch/Search/entity.html.twig', array(
            'string' => $string,
            'res'    => $entity,
            'entity' => $entityName ?? null,
            'page'   => $page,
            'locale' => $trans->getLocale(),
        ));

    }

}
